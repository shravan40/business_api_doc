# ChangeStreet Business API Document begins



# API Document begin here

> ##### Base URL -> https://api.moneyup.in/business_test

Request Type -> `GET`

Request URL -> `/`

Request Param ->

Response Param ->
```
{
    "message":"Welcome to ChangeStreet Business solution Api platform"
}
```
##### All the below Apis will be have version number v1. Kindly update the base URL as https://api.moneyup.in/business_test/v1/. Most common status codes are 201, 200, 400, 409, 500. One need to take action based on message recieved in response.

## Headers details
```
{
    "Origin" : "String Value"
    "Authorization" : "String Value"
}
```

##### There is no API to update employer data. Kindly double check the data fields before registering.

Request Type -> `POST`

Request URL -> `employer_registration`

Request Param ->
```
{
  "employer": {
    "name": "Your Company Name",
    "email": "user@example.com",
    "phone": "Mobile / Landline number",
    "password": "Strong format password of minimum length 12",
    "cin": "Company CIN",
    "pan": "Company PAN",
    "tan": "Company TAN",
    "url": "https://www.example.com"
  },
  "address": {
    "line_1": "Address line 1",
    "line_2": "Address line 2",
    "line_3": "Address line 3",
    "city": "City Name",
    "state": "State",
    "country": "India",
    "pin_code": "Area Pin code",
    "add_type": "Registered / Office / Current / Permanent"
  },
  "bank_detail": {
    "holder_name": "Account Holder Name",
    "bank_name": "Bank Name",
    "ifsc": "IFSC Code",
    "branch": "Branch Name",
    "account_type": "Savings / Current",
    "number": "Bank account number"
  }
}
```
Response Param ->
```
{
    "message" : "Registration successful, login to continue"
}
```



Request Type -> `POST`

Request URL -> `login`

Request Param ->
```
{
  "email": "admin@example.com",
  "password": "Strong format password of minimum length 12"
}
```

Response Param ->
```
{
  "message": "Login successful, use Auth token to access the apis.",
  "employer_id": 1,
  "auth_toke": "RARZvRmhrdSN9B*NHYqKJbFkxqW&qN93z8M#ehPTJRB6As82^#"
}
```



Request Type -> `POST`

Request URL -> `change_password`

Authentication Required
Request Param ->
```
{
  "employer_id": 1,
  "old_password": "Existing Password",
  "new_password": "Strong format password of minimum length 12"
}
```
Response Param ->
```
{
  "message": "Password change successful"
}
```



Request Type -> `POST`

Request URL -> `reset_password`

Request Param ->
```
{
  "email": "admin@example.com",
  "phone": "Mobile / Landline number",
  "cin": "Company CIN",
  "pan": "Company Pan"
}
```
Response Param ->
```
{
  "message": "Password has been succesfully reset, Please use this password for further commnucation",
  "password": "NHYqKJbFkxqW&qN9"
}
```



Request Type -> `POST`

Request URL -> `employee_onboarding`

Authentication Required
Request Param ->
```
{
  "employer_id": 1,
  "employees": [
    {
      "f_name": "First Name of Employee",
      "m_name": "Middle Name of Employee",
      "l_name": "Last Name of Employee",
      "email": "employee_x@example.com",
      "phone": "Mobile / Landline number",
      "pan": "Employee PAN",
      "aadhaar": "Employee Aadhar Number",
      "birth_date": "YYYY-MM-DD",
      "income_slab": "Below 1 Lac, 1-5 Lacs, 5-10 Lacs, 10-25 Lacs, 25 Lacs - 1 Crore, Above 1 Crore",
      "address": {
        "line_1": "Address line 1",
        "line_2": "Address line 2",
        "line_3": "Address line 3",
        "city": "City Name",
        "state": "State",
        "country": "India",
        "pin_code": "Area Pin code",
        "add_type": "Registered / Office / Current / Permanent"
      },
      "bank_details": {
        "holder_name": "Account Holder Name",
        "bank_name": "Bank Name",
        "ifsc": "IFSC Code",
        "branch": "Branch Name",
        "account_type": "Savings / Current",
        "number": "Bank account number"
      }
    }
  ]
}
```
Response Param ->
```
{
  "message": "Onboarding of employees are successful. Please capture the employee ids for use",
  "employee_details": [
    {
      "id": 12,
      "email": "user@example.com",
      "message": "Employee registered at ChangeStreet platform. Please find the unique id for future references",
      "phone_status": false,
      "email_status": false
    }
  ]
}
```



Request Type -> `POST`

Request URL -> `update_employee_data`

Authentication Required
Request Param ->
```
{
    "employer_id": 1,
    "employee_id": 5,
    "f_name": "First Name of Employee",
    "m_name": "Middle Name of Employee",
    "l_name": "Last Name of Employee",
    "email": "employee_x@example.com",
    "phone": "Mobile / Landline number",
    "pan": "Employee PAN",
    "aadhaar": "Employee Aadhar Number",
    "birth_date": "YYYY-MM-DD",
    "income_slab": "Below 1 Lac, 1-5 Lacs, 5-10 Lacs, 10-25 Lacs, 25 Lacs - 1 Crore, Above 1 Crore",
    "address": {
        "line_1": "Address line 1",
        "line_2": "Address line 2",
        "line_3": "Address line 3",
        "city": "City Name",
        "state": "State",
        "country": "India",
        "pin_code": "Area Pin code",
        "add_type": "Registered / Office / Current / Permanent"
    },
    "bank_details": {
        "holder_name": "Account Holder Name",
        "bank_name": "Bank Name",
        "ifsc": "IFSC Code",
        "branch": "Branch Name",
        "account_type": "Savings / Current",
        "number": "Bank account number"
    }
}
```
Response Param ->
```
{
    "message": "Employee data has been updated successfully",
    "update_status": Boolean Value  
}
```

Request Type -> `POST`

Request URL -> `kyc_check`

Authentication Required

Request Param ->

Response Param ->
```
{
  "message": "KYC details of all the Employees will be emailed to registered Email",
  "kyc_verified_employees": [
    [
      1,
      2
    ]
  ]
}
```

Request Type -> `GET`

Request URL -> `kyc_check_pan/<PAN>`

Request Param ->

Response Param ->
```
{
  "kyc_status": true,
  "message": "Given PAN holder is C-KYC compliant"
}
```

Request Type -> `POST`

Request URL -> `employee_folio`

Authentication Required

Request Param ->
```
{
    "employer_id": 1,
    "employee_ids": [23,32]
}
```
Response Param ->
```
{
    "employee_folio" = [
        {
            "folio_number": folio_number,
            "message": "Please keep this folio number for future references"
        }
    ]
}
```


Request Type -> `POST`

Request URL -> `set_goal`

Authentication Required

Request Param ->
```
{
  "employee_id": 1,
  "goal": {
    "name": "Home Loan",
    "amount": 10000
  }
}
```
Response Param ->
```
{
  "goal_id": 1234,
  "message": "Employee goal has been set"
}
```

Request Type -> `POST`

Request URL -> `update_goal`

Authentication Required

Request Param ->
```
{
  "employee_id": 1,
  "goal": {
    "id": 12,
    "name": "Home Loan",
    "amount": 10000
  }
}
```
Response Param ->
```
{
    "message": "Employee goal has been updated"
}
```


Request Type -> `POST`

Request URL -> `make_investment`

Authentication Required

Request Param ->
```
{
  "employer_id": 1,
  "investment": [
    {
      "employee_id": 1,
      "amount": 500,
      "goal_id": 10
    }
  ]
}
```
Response Param ->
```
{
  "global_order_id": 1000,
  "message": "Employees investments are in progress, You will be receiving an email with payment details."
}
```

Request Type -> `POST`

Request URL -> `payment_confirmation`

Authentication Required

Request Param ->
```
{
  "employer_id": 1,
  "order_id": 1,
  "reference_number": "RR91215100X",
  "amount": 25000
}
```
Response Param ->
```
{
  "message": "Payment details is sent to Mutual Fund house for confirmation"
}
```

Request Type -> `POST`

Request URL -> `investment_allotment_details`

Authentication Required

Request Param ->
```
{
    "employer_id" : Integer value,
    "global_order_id" : Integer value
}
```
Response Param ->
```
{
    "messsage" : string_value,
    "allotment_details" : [  
        {
            "amount" : double_value,
            "unit" : double_value,
            "purchase_nav" : double_value,
            "scheme_code" : string_value,
            "status" : string_value,
            "trade_status" : string_value,
            "employee_id" : integer_value,
            "goal_id" : integer_value
        }
    ]
}
```


Request Type -> `POST`

Request URL -> `redemption`

Authentication Required

Request Param ->
```
{
  "employer_id": 1,
  "employee_id": 1,
  "goal_id": 1,
  "amount": 1250,
  "redemption_flag": "FUll"
}
```
Response Param ->
```
{
  "message": "Current account value is less than requested amount"
}
```

Request Type -> `GET`

Request URL -> `latest_nav`

Request Param ->
Response Param ->
```
{
    "latest_nav" : [
         {
              "scheme_name" : string_value,
              "scheme_code" : string_value,
              "nav" : double_value,
              "nav_date" : string_value,
         }
    ]
}
```
